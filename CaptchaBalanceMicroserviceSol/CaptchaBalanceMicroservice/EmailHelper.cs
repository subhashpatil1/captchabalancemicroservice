﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CaptchaBalanceMicroservice
{
    public static class EmailHelper
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        #region CommonFunctions

        /// <summary>
        /// Common function for Sending an email
        /// </summary>
        /// <param name="FromEmail"></param>
        /// <param name="ToEmail"></param>
        /// <param name="EmailSubject"></param>
        /// <param name="EmailBody"></param>
        /// <param name="CcEmail"></param>
        public static void SendEmail(string FromEmail, string ToEmail, string EmailSubject, string EmailBody, string CcEmail)
        {
            string defaultEmailId = "Subhash.Patil@sterlingts.com";
            try
            {
                EmailBody = EmailBody.Replace("##br##", "</br>"); ;
                FromEmail = string.IsNullOrEmpty(FromEmail) ? defaultEmailId : FromEmail;
                ToEmail = string.IsNullOrEmpty(ToEmail) ? defaultEmailId : ToEmail;

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(FromEmail);
                //Adding To list
                foreach (var address in ToEmail.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mailMessage.To.Add(address);
                }

                //Adding CC list
                if (!string.IsNullOrEmpty(CcEmail))
                {
                    foreach (var address in CcEmail.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        mailMessage.CC.Add(address);
                    }
                }
                mailMessage.Subject = EmailSubject;
                mailMessage.Body = EmailBody;
                mailMessage.IsBodyHtml = true;
               // var loginInfo = new System.Net.NetworkCredential(FromEmail, "");

                using (var smtpClient = new SmtpClient("smtp.st.com", 25))
                {
                    smtpClient.EnableSsl = false;
                    //smtpClient.UseDefaultCredentials = false;
                    //smtpClient.Credentials = loginInfo;
                    smtpClient.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error sending email to " + ToEmail + ". Error :- " + ex.Message, true);
                throw ex;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Common method to log messages (log4Net logger).
        /// </summary>
        /// <param name="Msg"></param>
        /// <param name="isError"></param>
        public static void LogMessage(string Msg, bool isError)
        {
            log4net.Config.XmlConfigurator.Configure();
            if (!isError)
            {
                logger.Info(Msg);
            }
            else
            {
                logger.Error(Msg);
            }
        }
        #endregion

    }
}
