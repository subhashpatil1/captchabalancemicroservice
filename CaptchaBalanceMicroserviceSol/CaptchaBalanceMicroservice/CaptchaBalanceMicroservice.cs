﻿// ***********************************************************************
// Assembly         : PRISMDataAdaptor
// Author           : Subhash Patil
// Created          : 01-29-2019
//
// Last Modified By : Subhash Patil
// Last Modified On : 01-29-2019
// ***********************************************************************
// <copyright file="Fulfillment.cs" company="Sterling Talent Solutions">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************

/* 'Modification log
/'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/' Defect No.     Date (mm/dd/yyyy)       Author            Description
/'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/                   01/29/2019           Subhash Patil        Service Created
/ </summary>
*/
using DeathByCaptcha;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using AntiCaptcha;

namespace CaptchaBalanceMicroservice
{
    public partial class CaptchaBalanceMicroservice : ServiceBase
    {
        private static System.Timers.Timer balanceTimer;
        private static string emailTo;
        private static string emailFrom;
        private static string emailSubject;
        private static double dbcThreshold;
        private static double twocaptchaThreshold;
        private static double antiCaptchaThreshold;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public CaptchaBalanceMicroservice()
        {
            InitializeComponent();
            balanceTimer = new System.Timers.Timer(Convert.ToDouble(ConfigurationManager.AppSettings["TimerIntervalMinutes"]) * 60000);
            dbcThreshold = Convert.ToDouble(ConfigurationManager.AppSettings["dbcThreshold"]);
            twocaptchaThreshold = Convert.ToDouble(ConfigurationManager.AppSettings["twoCaptchaThreshold"]);
            antiCaptchaThreshold = Convert.ToDouble(ConfigurationManager.AppSettings["antiCaptchaThreshold"]);
            emailFrom = ConfigurationManager.AppSettings["EmailFrom"];
            emailTo = ConfigurationManager.AppSettings["ToEmail"];
            emailSubject = ConfigurationManager.AppSettings["EmailSubject"];
        }

        public static System.Timers.Timer BalanceTimer
        {
            get
            {
                return balanceTimer;
            }

            set
            {
                balanceTimer = value;
            }
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            Log.Info("Captcha Balance Service started." + DateTime.Now.ToString());
            var worker = new Thread(Execute);
            worker.Name = "CaptchaBalanceService";
            worker.IsBackground = false;
            worker.Start();

            //Execute();
        }

        protected override void OnStop()
        {
            balanceTimer.Dispose();
            Log.Info("Captcha Balance Service stopped." + DateTime.Now.ToString());
        }

        public void Execute()
        {
            try
            {
                System.Timers.ElapsedEventHandler elapsedeventinsert = GetCaptchaBalanceEvent;
                balanceTimer.AutoReset = true;
                balanceTimer.Enabled = true;
                balanceTimer.Elapsed += elapsedeventinsert;
                balanceTimer.Start();
                Log.Info("BalanceTimer started " + System.Threading.Thread.CurrentThread.ManagedThreadId);
            }
            catch (System.Exception ex)
            {
                Log.Error("Error occured in Captcha Balance Service.", ex);
            }
        }

        private void GetCaptchaBalanceEvent(object sender, ElapsedEventArgs e)
        {
            double balance = 0, threshhold = Convert.ToDouble(ConfigurationManager.AppSettings["TimerIntervalMinutes"]);
            string emailBody = string.Empty;
            try
            {
                balanceTimer.Stop();

                #region DBC captcha Balance Check
                Log.Info("Getting DBC balance event started");
                HttpClient client = new HttpClient(ConfigurationManager.AppSettings["dbcUsername"], ConfigurationManager.AppSettings["dbcPassword"]);

                //Getting Balance
                balance = client.Balance / 100;
                Console.WriteLine("DBC Balance : $" + balance);
                InsertBalance(balance, "A-DBC");
                if (dbcThreshold > balance)
                {
                    emailBody = "<tr><td>Hi,</td></tr><tr><td>##br##Below are balance details for captcha##br##<td></tr><tr><td>" + "Current DBC Balance: $" + balance + "</td></tr>##br####br####br##";
                    generateAlert("DeathByCaptcha", emailBody);
                }
                Console.WriteLine("Getting DBC balance event Completed");
                #endregion

                #region 2 captcha Balance Check                
                Log.Info("Getting 2 captcha balance event started");

                WebRequest req = WebRequest.Create(ConfigurationManager.AppSettings["2captchaBalanceUrl"].Replace("&amp;", "&"));
                WebResponse resp = req.GetResponse();
                using (StreamReader answerStream = new StreamReader(resp.GetResponseStream()))
                {
                    string answerResponse = answerStream.ReadToEnd();
                    if (answerResponse == null)
                    {
                        Log.Error("Exception occured in 2Captcha balance");
                        emailBody = "<tr><td>Hi,</td></tr><tr><td>##br##Exception occured in 2Captcha balance##br##<td></tr>";
                        generateAlert("Error Occured", emailBody);
                    }
                    else
                    {
                        balance = double.Parse(answerResponse.ToString());
                        Console.WriteLine("2Captcha Balance : $" + balance);
                        InsertBalance(balance, "2Captcha");
                        if (twocaptchaThreshold > balance)
                        {
                            emailBody = "<tr><td>Hi,</td></tr><tr><td>##br##Below are balance details for captcha##br##<td></tr><tr><td>" + "Current 2Captcha Balance: $" + balance + "</td></tr>##br####br####br##";
                            generateAlert("2Captcha", emailBody);
                        }
                    }
                }
                #endregion

                #region Anticaptcha Balance Check
                /*
                var api = new AntiCaptcha.Api.ImageToText();                
                api.ClientKey = ConfigurationManager.AppSettings["AntiCaptchaApiKey"];
                var balanceResponse = api.GetBalance();
                if (balanceResponse == null)
                {
                    Log.Error("Exception occured in AntiCaptcha balance");
                    emailBody = "<tr><td>Hi,</td></tr><tr><td>##br##Exception occured in AntiCaptcha balance##br##<td></tr>";
                    generateAlert("Error Occured", emailBody);
                }
                else
                {
                    balance = double.Parse(balanceResponse.ToString());
                    Console.WriteLine("AntiCaptcha Balance : $" + balance);
                    InsertBalance(balance, "AntiCaptcha");
                    if (antiCaptchaThreshold > balance)
                    {
                        emailBody = "<tr><td>Hi,</td></tr><tr><td>##br##Below are balance details for captcha##br##<td></tr><tr><td>" + "Current AntiCaptcha Balance: $" + balance + "</td></tr>##br####br####br##";
                        generateAlert("AntiCaptcha", emailBody);
                    }
                }
                */
                #endregion

            }
            catch (System.Exception ex)
            {
                Log.Error("Exception occured in GetDBCBalanceEvent.", ex);
            }
            finally
            {
                balanceTimer.Start();
            }
        }
        
        private void InsertBalance(double balance, string CaptchaType)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PRISMAzureSQLDBConn"].ConnectionString))
            {
                con.Open();
                using (SqlCommand sqlCommand = new SqlCommand("Insert into captchaBalance(CaptchaType,Balance,UpdatedOn) values(@CaptchaType,@Balance,getutcdate())", con))
                {
                    sqlCommand.Parameters.AddWithValue("@CaptchaType", CaptchaType.ToString());
                    sqlCommand.Parameters.AddWithValue("@Balance", Math.Round(balance, 2));
                    sqlCommand.ExecuteNonQuery();
                }
                con.Close();
            }
        }

        private void generateAlert(string type, string body)
        {
            try
            {
                EmailHelper.SendEmail(emailFrom, emailTo, type + emailSubject, body, "");
            }
            catch (System.Exception ex)
            {
                Log.Error("Exception occured in generateAlert.", ex);
            }
        }
    }
}
